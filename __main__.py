import numpy as np
from sklearn.datasets import fetch_openml
from sklearn.decomposition import PCA
from sklearn.model_selection import train_test_split
from sklearn.cluster import KMeans
from sklearn.metrics import silhouette_score
import matplotlib as mpl
import matplotlib.pyplot as plt

mpl.rc('axes', labelsize=14)
mpl.rc('xtick', labelsize=12)
mpl.rc('ytick', labelsize=12)

np.random.seed(42)

mnist = fetch_openml('mnist_784', version=1, cache=True, as_frame=False)

X = mnist["data"]
y = mnist["target"].astype(np.uint8)

# Dataset contains 70000 samples. Split 10000 and then again 10000 for validation and test.
# The remaining 50000 is for training.
X_temp, X_test, y_temp, y_test = train_test_split(X, y, test_size=10000, random_state=42)
X_train, X_val, y_train, y_val = train_test_split(X_temp, y_temp, test_size=10000, random_state=42)

print("Training set shape before PCA:", X_train.shape)

# Apply PCA for dimensionality reduction
pca = PCA(n_components=0.95)
X_train_reduced = pca.fit_transform(X_train)
X_val_reduced = pca.transform(X_val)
X_test_reduced = pca.transform(X_test)

print("Training set shape after PCA:", X_train_reduced.shape)

# Clustering and finding optimal cluster number
kmeans_k = range(5, 50, 5)
inertias = []
silhouette_scores = []

for k in kmeans_k:
    kmeans = KMeans(n_clusters=k, random_state=42)
    kmeans.fit(X_train_reduced)
    inertias.append(kmeans.inertia_)
    silhouette_scores.append(silhouette_score(X_train_reduced, kmeans.labels_))

# Plotting the inertia to use the elbow method
plt.figure(figsize=(8, 4))
plt.plot(kmeans_k, inertias, 'bo-')
plt.xlabel('Number of clusters (k)')
plt.ylabel('Inertia')
plt.title('Elbow Method for Optimal k')
plt.show()

# Plotting the silhouette scores
plt.figure(figsize=(8, 4))
plt.plot(kmeans_k, silhouette_scores, 'bo-')
plt.xlabel('Number of clusters (k)')
plt.ylabel('Silhouette score')
plt.title('Silhouette Score for Optimal k')
plt.show()
